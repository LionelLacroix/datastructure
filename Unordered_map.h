#include <iostream>
#include <vector>


template <typename Key,typename T,typename U>
class Iterator {
public:
    Key first;
    T second;
    Iterator(U* pdata_):pdata(pdata_),prevElt(nullptr),
        nextElt(nullptr),next(nullptr),first(Key()),second(T()){
    }

    Iterator(U* pdata_, const Key & key, const T& value):pdata(pdata_),
        first(key),second(value) {
    }

    Iterator<Key,T,U>& begin()
    {
        return *pdata->begin;
    }

    bool operator==(Iterator<Key,T,U> &other)
    {
        return first==other.first;
    }

    bool operator!=(Iterator<Key,T,U> &other)
    {
        return first!=other.first;
    }

    Iterator<Key,T,U>& Next()
    {
        if(nextElt)
            return *nextElt;
        return pdata->end();
    }

    Iterator<Key,T,U>& Prev()
    {
        if(prevElt)
            return *prevElt;
        return pdata->end();
    }

public:
    Iterator<Key,T,U>* prevElt;
    Iterator<Key,T,U>* nextElt;
    Iterator<Key,T,U>* next;
private:
    U* pdata;
};


template <typename Key,typename T>
class Unordered_map {
    friend class Iterator<Key,T,Unordered_map>;
private:
    static const int SIZE = 500;
    Iterator<Key,T,Unordered_map>* dataMap[SIZE];
    Iterator<Key,T,Unordered_map>* begin;
    Iterator<Key,T,Unordered_map>* last;
    Iterator<Key,T,Unordered_map>* m_end;
    size_t length;

    int hash(const Key& key) { //simple hash function
        int hash = 0;
        for (size_t i = 0; i < key.length(); i++) {
            int asciiValue = int(key[i]);
            hash = (hash + asciiValue * 23) % SIZE;
        }
        return hash;
    }

public:
     Unordered_map():dataMap{nullptr}, begin(nullptr),
         m_end(nullptr),last(nullptr),length(0) {
         m_end = new Iterator<Key,T,Unordered_map>(this);
     }

     bool isEmpty()
     {
         return length==0;
     }

     size_t size()
     {
         return length;
     }

     Iterator<Key,T,Unordered_map> iterator()
     {
         if(begin)
            return *begin;
         return *m_end;
     }

     Iterator<Key,T,Unordered_map>& end()
     {
         return *m_end;
     }

     Iterator<Key,T,Unordered_map>& find(const Key &key)
     {
         int index = hash(key);
         Iterator<Key,T,Unordered_map>* temp = dataMap[index];
         while (temp) {
             if (temp->first == key) return *temp;
             temp = temp->next;
         }
        return *m_end;
     }

    ~Unordered_map() {
        for (int i = 0; i < SIZE; i++) {
            Iterator<Key,T,Unordered_map>* head = dataMap[i];
            Iterator<Key,T,Unordered_map>* temp = head;
            //while (head) {
            //    head = head->nextElt;
            //    delete temp;
            //    temp = head;
            //}
        }
        delete m_end;
    }

    void printTable() {
        for (int i = 0; i < SIZE; i++) {
            std::cout << i << ":" << std::endl;
            if (dataMap[i]) {
                Iterator<Key,T,Unordered_map>* temp = dataMap[i];
                while (temp) {
                    std::cout << "   {" << temp->key << ", " << temp->value << "}" << std::endl;
                    temp = temp->next;
                }
            }
        }
    }

    void insert(const std::pair<Key,T>& value) {
        int index = hash(value.first);

        if (length == 0) {
            Iterator<Key,T,Unordered_map>* newNode = new Iterator<Key,T,Unordered_map>(this,value.first, value.second);
            last = newNode;
            begin = newNode;
            dataMap[index] = newNode;
             length++;
            return;
        }
        if (dataMap[index] == nullptr) {
            Iterator<Key,T,Unordered_map>* newNode = new Iterator<Key,T,Unordered_map>(this,value.first, value.second);
            dataMap[index] = newNode;
            newNode->prevElt = last;
            last->nextElt = newNode;
            last = newNode;
            length++;
        }
        else
        {   //collision case
            Iterator<Key,T,Unordered_map>* temp = dataMap[index];
            while (temp->next != nullptr && temp->first != value.first) {
                temp = temp->next;
            }
            if(!temp->next)
            {
                Iterator<Key,T,Unordered_map>* newNode = new Iterator<Key,T,Unordered_map>(this,value.first, value.second);
                temp->next = newNode;
                newNode->prevElt = last;
                if(last)
                    last->nextElt = newNode;
                last = newNode;
                length++;
            }
            else
                temp->next->second = value.second;
        }
    }

    T at(const Key& key) {
        return find(key).second;
    }

    std::vector<std::string> keys() {
        std::vector<std::string> allKeys;
         Iterator<Key,T,Unordered_map>* temp = begin;
         allKeys.push_back(temp->key);
        while(temp->nextElt) {
               temp = temp->nextElt;
                allKeys.push_back(temp->key);
            }
        return allKeys;
        }
};
