#include <iostream>

template <typename Key,typename T,typename U>
class Iterator {
  public:
    Key first;
    T second;

    Iterator(U* pdata_, const Key & key, const T& value):pdata(pdata_),
        first(key),second(value),left(nullptr),
        right(nullptr), next(nullptr), prev(nullptr) {
    }

    Iterator<Key,T,U>& begin()
    {
        return *pdata->root;
    }


    Iterator<Key,T,U>& Next()
    {
        return *next;
    }

    Iterator<Key,T,U>& Prev()
    {
        return *prev;
    }

    bool operator==(Iterator<Key,T,U> &other)
    {
        return first==other.first;
    }

    bool operator!=(Iterator<Key,T,U> &other)
    {
        return first!=other.first;
    }

    Iterator<Key,T,U>* left;
    Iterator<Key,T,U>* right;
    Iterator<Key,T,U>* next;
    Iterator<Key,T,U>* prev;
    private:
        U* pdata;
};

template <typename Key, typename T>
class Map {
      friend class Iterator<Key,T,Map>;
private:
    Iterator<Key,T,Map>* root;
    Iterator<Key,T,Map>* last;
    size_t length;
public:
    Map():root(nullptr),last(nullptr),length(0) {  }


    void erase(Iterator<Key,T,Map>* currentNode) {
        if(currentNode && currentNode->next && currentNode->next->prev)
            currentNode->next->prev = currentNode->prev;
        if(currentNode && currentNode->prev && currentNode->prev->next && currentNode->next)
            currentNode->prev->next = currentNode->next;
        if (currentNode->left)
            erase(currentNode->left);
        if (currentNode->right)
            erase(currentNode->right);
        delete currentNode;
        length--;
    }

    ~Map() { clear();}

    size_t size() {
        return length;
    }

    bool isEmpty()
    {
        return length==0;
    }

    Iterator<Key,T,Map>& end()
    {
        return *last;
    }

    Iterator<Key,T,Map>& iterator()
    {
        return *root;
    }
    void clear()
    {
       auto * temp = root;
       Iterator<Key,T,Map>* tmp;
        while(temp)
        {
            tmp = temp->next;
            delete temp;
            temp = tmp;
        }
        length = 0;
    }

    T operator[](Key key)
    {
        auto itFound = find(key);
        if(itFound != end())
            return itFound.second;
        return T();
    }


    bool insert(const std::pair<Key,T>& value) {
        Iterator<Key,T,Map>* newNode = new Iterator<Key,T,Map>(this,value.first, value.second);

        newNode->prev = last;
        if(last)
            last->next = newNode;
        last = newNode;
        length++;
        if (root == nullptr) {
            root = newNode;
            return true;
        }
        Iterator<Key,T,Map>* temp = root;
        while (true) {
            if (newNode->first == temp->first)
            {
                length--;
                return false;
            }
            if (newNode->first < temp->first) {
                if (temp->left == nullptr) {
                    temp->left = newNode;
                    return true;
                }
                temp = temp->left;
            }
            else {
                if (temp->right == nullptr) {
                    temp->right = newNode;
                    return true;
                }
                temp = temp->right;
            }
        }
    }

    Iterator<Key,T,Map>& find(const Key &key) {
        if (root == nullptr) return *last;
        Iterator<Key,T,Map>* temp = root;
        while (temp) {
            if (key < temp->first) {
                temp = temp->left;
            }
            else if (key > temp->first) {
                temp = temp->right;
            }
            else {
                return *temp;
            }
        }
        return *last;
    }
};
