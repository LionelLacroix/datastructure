#include <iostream>


template <typename T, typename U>
class Iterator {
public:
    Iterator(U* container_,T &value_):container(container_),value(value_),next(nullptr),prev(nullptr) {
    }
    Iterator<T,U>* Next()
    {
        return next;
    }
    Iterator<T,U>* Prev()
    {
        return prev;
    }
    Iterator<T,U>* begin()
    {
        return container->head;
    }
    T current()
    {
        return value;
    }

public:
    T value;
    U* container;
    Iterator<T,U>* prev;
    Iterator<T,U>* next;

};

template <typename T>
class List {
    friend Iterator<T, List>;
private:
    Iterator<T, List>* head;
    Iterator<T, List>* tail;
    int length;

public:
    List():head(nullptr),tail(nullptr),length(0) {
    }

    ~List() {
        Iterator<T,List>* temp = head;
        Iterator<T,List>* tmp;
        while (temp->next) {
           tmp = temp->next;
           delete temp;
           temp = tmp;
        }
    }

    void printList() {
        Iterator<T, List>* temp = head;
        while (temp != nullptr) {
            std::cout << temp->value << std::endl;
            temp = temp->next;
        }
    }

    T front() {
        if (head != nullptr)
            return head->current();
          return T();
    }

    T last() {
        if (tail != nullptr)
           return tail->current();
        return T();
    }

    Iterator<T,List>* iterator()
    {
      return  head;
    }

    void resize(size_t n)
    {
        for(size_t i = length ; i < n; ++i)
            push_back(T());
    }

    bool isEmpty() {
       return length==0;
    }
    size_t size() {
       return length;;
    }

    void push_back(T value) {
        Iterator<T,List>* newNode = new Iterator<T,List>(this,value);
        if (length == 0) {
            head = newNode;
            tail = newNode;
        }
        else {
            tail->next = newNode;
            newNode->prev = tail;
            tail = newNode;
        }
        length++;
    }

    void pop_last() {
        if (length == 0) return;
        Iterator<T,List>* temp = tail;
        if (length == 1) {
            head = nullptr;
            tail = nullptr;
        }
        else {
            tail = tail->prev;
            tail->next = nullptr;
        }
        delete temp;
        length--;
    }

    void push_front(T value) {
        Iterator<T, List>* newNode = new Iterator<T,List>(this,value);
        if (length == 0) {
            head = newNode;
            tail = newNode;
        }
        else {
            newNode->next = head;
            head->prev = newNode;
            head = newNode;
        }
        length++;
    }

    void pop_front() {
        if (length == 0) return;
        Iterator<T, List>* temp = head;
        if (length == 1) {
            head = nullptr;
            tail = nullptr;
        }
        else {
            head = head->next;
            Iterator<T,List>* tmp  = head->prev;
            tmp = nullptr;
        }
        delete temp;
        length--;
    }

    Iterator<T,List>* get(int index) {
        if (index < 0 || index >= length) return nullptr;
        Iterator<T,List>* temp = head;
        if (index < length / 2) {
            for (int i = 0; i < index; ++i) {
                temp = temp->next;
            }
        }
        else {
            temp = tail;
            for (int i = length - 1; i > index; --i) {
                temp = temp->prev;
            }
        }
        return temp;
    }

    T operator[](size_t i )
    {
        Iterator<T,List>* it= get(i);
        if(it)
          return it->current();
        return tail->current();
    }

    bool set(int index, int value) {
        Iterator<T,List>* temp = get(index);
        if (temp) {
            temp->value = value;
            return true;
        }
        return false;
    }

    bool insert(int index, int value) {
        if (index < 0 || index > length) return false;
        if (index == 0) {
            push_front(value);
            return true;
        }
        if (index == length) {
            push_back(value);
            return true;
        }

        Iterator<T,List>* newNode = new Iterator<T,List>(this,value);
        Iterator<T,List>* before = get(index - 1);
        Iterator<T,List>* after = before->next;
        newNode->prev = before;
        before->next = newNode;
        after->prev = newNode;
        length++;
        return true;
    }

    void erase(Iterator<T,List>* temp ){
        if(temp && temp->next && temp->prev )
            temp->next->prev = temp->prev;
        if(temp && temp->prev && temp->next)
            temp->prev->next = temp->next;
        delete temp;
        length--;
    }

    void erase(int index) {
        if (index < 0 || index >= length) return;
        if (index == 0)  return pop_front();
        if (index == length - 1)  return pop_last();
        erase(get(index));
    }
};


