#include <iostream>
#include <climits>


template <typename T, typename U>
class Iterator {
public:
    Iterator(U* p_data_):value(T()),pdata(p_data_),next(nullptr),prev(nullptr) {
    }
    Iterator(U* p_data_,T &value_):value(value_),pdata(p_data_),next(nullptr),prev(nullptr) {
    }
    Iterator<T,U>& Next()
    {
        if(next)
            return *next;
        return *pdata->m_end;
    }
    Iterator<T,U>& Prev()
    {
        if(prev)
            return *prev;
        return *pdata->m_end;
    }
    Iterator<T,U>& begin()
    {
        if(pdata && pdata->first)
            return *pdata->first;
        return *pdata->m_end;
    }
    const T current()
    {
        return value;
    }

public:
    Iterator* next;
    Iterator* prev;
    T value;
private:
    U* pdata;

};


template <typename T>
class Queue {
    friend class Iterator<T,Queue>;
private:
    Iterator<T,Queue>* first;
    Iterator<T,Queue>* last;
    Iterator<T,Queue>* m_end;
    size_t length;

public:
    Queue():length(0), first(nullptr),last(nullptr),
    m_end(nullptr){
        m_end = new Iterator<T,Queue>(this);
    }

    void printQueue() {
         Iterator<T,Queue>* temp = first;
        while (temp) {
            std::cout << temp->value << std::endl;
            temp = temp->next;
        }
    }


    T Front() {
        if (first)
            return first->value;
        return T();
    }

    Iterator<T,Queue>&  end()
    {
        return *m_end;
    }
    Iterator<T,Queue>& iterator()
    {
        if(first)
            return *first;
        return *m_end;

    }

    T Last() {
        if(last)
           return last->value;
        return T();
    }

    size_t size() {
        return length;
    }

    bool isEmpty() {
        if (length == 0) return true;
        return false;
    }

    void push(int value) {
        Iterator<T,Queue>* newNode = new Iterator<T,Queue>(this,value);
        if (length == 0) {
            first = newNode;
            last = newNode;
        }
        else {
            last->next = newNode;
            newNode->prev = last;
            last = newNode;
        }
        length++;
    }

    T pop() {
        if (length == 0) return INT_MIN;
        Iterator<T,Queue>* temp = first;
        int dequeuedValue = first->value;
        if (length == 1) {
            first = nullptr;
            last = nullptr;
        }
        else {
            first = first->next;
            first->prev = nullptr;
        }
        delete temp;
        length--;
        return dequeuedValue;
    }

};
