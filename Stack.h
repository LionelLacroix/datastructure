#include <iostream>
#include <climits>

template <typename T, typename U>
class Iterator {
public:
    Iterator(U* p_data_):value(T()),pdata(p_data_),next(nullptr),prev(nullptr) {
    }
    Iterator(U* p_data_,T &value_):value(value_),pdata(p_data_),next(nullptr),prev(nullptr) {
    }

    Iterator<T,U>& Next()
    {
        if(next)
            return *next;
        return *pdata->m_end;
    }

    Iterator<T,U>& Prev()
    {
        if(prev)
            return *prev;
        return *pdata->m_end;

    }

    Iterator<T,U>& begin()
    {
        if(pdata->top)
            *pdata->top;
        return *pdata->m_end;
    }

    const T current()
    {
        return value;
    }
private:
    U* pdata;

public:
    T value;
    Iterator* next;
    Iterator* prev;
};


template <typename T>
class Stack {
    friend class Iterator<T,Stack>;
private:
    Iterator<T,Stack>* top;
    int height;
    Iterator<T,Stack>* m_end;

public:
    Stack():height(0),top(nullptr),m_end(nullptr) {
        m_end = new Iterator<T,Stack>(this);
    }

    ~Stack() {
        Iterator<T,Stack>* temp = top;
        while (top) {
            top = top->next;
            delete temp;
            temp = top;
        }
    }

    void printStack() {
        Iterator<T,Stack>* temp = top;
        while (temp) {
            std::cout << temp->value << std::endl;
            temp = temp->next;
        }
    }

    T front() {
        if (top)
            return  top->value;
        return  T();
    }

    size_t size() {
        return height;
    }

    bool isEmpty() {
        if (height == 0) return true;
        return false;
    }

     Iterator<T,Stack>& iterator()
     {
         if(top)
            return *top;
        return *m_end;
     }

    void push(int value) {
        Iterator<T,Stack>* newNode = new Iterator<T,Stack>(this,value);
        if (height == 0) {
            top = newNode;
        }
        else
        {
            newNode->next = top;
            top->prev = newNode;
            top = newNode;
        }
        height++;
    }


    T pop_last() {
        if (height == 0) return T();

        Iterator<T,Stack>* temp = top;
        T poppedValue = top->value;
        top = top->next;
        delete temp;
        height--;
        return poppedValue;
    }
};
