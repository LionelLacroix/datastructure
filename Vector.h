#include <iostream>
#include <string>
#include <vector>
#include <memory>


template <typename T, typename U>
class Iterator {
 public:
  Iterator(U *p_data) : m_p_data_(p_data), m_currPos(0){
  }

  Iterator<T,U>& begin() {
   m_currPos = 0;
   return *this;
  }

  Iterator<T,U>& Next() {
      if(m_currPos< m_p_data_->m_currlen_)
          m_currPos++;

      return *this;
  }

  Iterator<T,U>& Prev() {
      if(m_currPos> 0)
          m_currPos--;
      return *this;
  }

  const T& end() {
     return m_end;
  }

  const T& Current() {
      if(m_currPos< m_p_data_->m_currlen_)
          return m_p_data_->m_data_.get()[m_currPos];
      else
          return end();
  }

 private:
  U* m_p_data_;
  size_t m_currPos;
  T m_end;
};



template <class T>
class Vector {
  friend class Iterator<T, Vector>;

 public:

  struct free_deleter
  {
      void operator()(void * p)
      {
          delete static_cast<T*>(p);
      }
  };

  void resize(size_t size )
  {
        reserve(size);
        m_currlen_ = size;
  }

  void reserve(size_t size )
  {
      m_maxlen_ = size;
      void* newData = realloc(m_data_.get(), m_maxlen_);
      if (m_data_) {
          m_data_.release();
      }
      m_data_.reset(static_cast<T*>(newData));
  }

  T& operator[](size_t i)
  {
      return m_data_.get()[i];
  }
  void push_back(T a) {
      if (m_currlen_ >= m_maxlen_) {
          m_maxlen_ *= 2;
          void* newData = realloc(m_data_.get(), m_maxlen_);

          if (m_data_) {
              m_data_.release();
          }
          m_data_.reset(static_cast<T*>(newData));
      }
      m_data_.get()[m_currlen_++] = a;
  }

  Iterator<T, Vector>& iterator()
  {
    return  m_it;
  }

  Vector(): m_maxlen_(1),
      m_currlen_(0),
      m_data_(new T(1)),
      m_it(this)
  {

  }

  size_t size(){return m_currlen_;}
  ~Vector()=default;

 private:
  size_t m_maxlen_;
  size_t m_currlen_;
  std::unique_ptr<T, free_deleter> m_data_;
  Iterator<T, Vector> m_it;
};


