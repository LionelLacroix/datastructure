
//n2 complexity
template <typename T>
void bubbleSort(T array[], int size)
{
    for (int i = size - 1; i > 0; i--)
    {
        for (int j = 0; j < i; j++)
            if (array[j] > array[j + 1])
            {
                T temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
    }
}

//n 2 complexity
template <typename T>
void selectionSort(T array[], int size) {
            for (int i = 0; i < size; i++) {
                int minIndex = i;
                for (int j = i+1; j < size; j++) {
                    if (array[j] < array[minIndex]) {
                        minIndex = j;
                    }
                }
                if (i != minIndex) {
                    T temp = array[i];
                    array[i] = array[minIndex];
                    array[minIndex] = temp;
                }
            }
        }
