#include "../Unordered_map.h"
#include <gtest/gtest.h>


TEST(UnorderedMap_insert, working_append)
{
    Unordered_map<std::string,int> mapping;
    mapping.insert(std::pair<std::string,int>("Test1",3));
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    ASSERT_TRUE(mapping.at("Test1") == 3);
    ASSERT_TRUE(mapping.at("Test4") == 8);
    ASSERT_FALSE(mapping.at("Test8") == 8);
}


TEST(UnorderedMapSize, working_size)
{
    Unordered_map<std::string,int> mapping;
    ASSERT_TRUE(mapping.isEmpty());
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    ASSERT_TRUE(mapping.size() == 2);
    //mapping.clear();
    //ASSERT_TRUE(mapping.isEmpty());
}


TEST(UnorderedMapIterator, working_Iterator)
{
    Unordered_map<std::string,int> mapping;
    mapping.insert(std::pair<std::string,int>("Test1",3));
    mapping.insert(std::pair<std::string,int>("Test2",4));
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    auto itFound = mapping.find("Test1");
    ASSERT_TRUE(itFound!= mapping.end());
    auto itFound1 = mapping.find("Test17");
    ASSERT_FALSE(itFound1!= mapping.end());

    auto it = mapping.iterator();
    ASSERT_TRUE(it.begin().second==3);
    ASSERT_TRUE(it.begin().first=="Test1");
    ASSERT_TRUE(it.begin().Next().second==4);
    ASSERT_TRUE(it.begin().Next().first=="Test2");
    ASSERT_TRUE(it.begin().Next().Prev().second==3);

}
