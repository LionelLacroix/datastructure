#include "../Queue.h"
#include <gtest/gtest.h>

TEST(QueuePush_back, working_push)
{
Queue<int> test;
test.push(1);
test.push(4);
test.push(5);

ASSERT_TRUE(test.Front()==1);
ASSERT_TRUE(test.Last()==5);
test.pop();
ASSERT_TRUE(test.Front()==4);
}


TEST(QueueSize, working_size)
{
Queue<int> test;
ASSERT_TRUE(test.isEmpty());
test.push(5);
test.push(2);
ASSERT_TRUE(test.size()==2);
ASSERT_FALSE(test.isEmpty());
}


TEST(QueueIterator, working_iterator)
{
  Queue<int> test;
  Iterator<int, Queue<int>> it = test.iterator();
  test.push(1);
  test.push(4);
  test.push(5);
  ASSERT_TRUE(it.begin().current()==1);
  ASSERT_TRUE(it.begin().Next().current()==4);
  ASSERT_TRUE(it.begin().Next().Prev().current()==1);
}

