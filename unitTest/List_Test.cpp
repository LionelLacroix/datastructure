#include "../List.h"
#include <gtest/gtest.h>


TEST(ListPush_back, working_push)
{
List<int> test;
test.push_back(1);
test.push_back(4);
test.push_back(5);
ASSERT_TRUE(test[1]==4);
test.push_front(40);
ASSERT_TRUE(test [3]==5);
ASSERT_TRUE(test[0] ==40);
}


TEST(ListSize, working_size)
{
List<double> test;
ASSERT_TRUE(test.isEmpty());
test.resize(5);
ASSERT_TRUE(test.size()==5);
}


TEST(ListAccess, working_size)
{
  List<int> test;
  test.push_back(1);
  test.push_back(4);
  test.push_back(5);
  test.push_back(40);
  test.pop_last();
  test.pop_front();
  ASSERT_TRUE(test.front()==4);
  ASSERT_TRUE(test.last()==5);
  test.insert(1,90);
  ASSERT_TRUE(test[1]==90);
  test.erase(1);
  ASSERT_TRUE(test[1] == 5);
}

TEST(ListIterator, working_iterator)
{
  List<int> test;
  test.push_back(1);
  test.push_back(4);
  test.push_back(5);
  Iterator<int,List<int>>* it = test.iterator();
  //int tmp =  it->current();
  ASSERT_TRUE(it->begin()->current()==1);
  ASSERT_TRUE(it->begin()->Next()->current()==4);
  ASSERT_TRUE(it->begin()->Next()->Prev()->current()==1);
}
