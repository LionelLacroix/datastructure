#include "../Vector.h"
#include <gtest/gtest.h>

TEST(VectorPush_back, working_append)
{

Vector<int> test;
test.reserve(4);
test.push_back(1);
test.push_back(4);
test.push_back(5);
test.push_back(40);
ASSERT_TRUE(test[1]==4);
ASSERT_TRUE(test [2]==5);
ASSERT_TRUE(test[3] ==40);
ASSERT_TRUE(test.size());
}


TEST(VectorResize, working_resize)
{
Vector<double> test;
test.resize(5);
ASSERT_TRUE(test.size()==5);
}


TEST(VectorIterator, working_iterator)
{
    Vector<float> test;
    test.push_back(1);
    test.push_back(4);
    test.push_back(5);
    Iterator<float, Vector<float>> it = test.iterator();
    ASSERT_TRUE(it.begin().Current()==1);
    ASSERT_TRUE(it.begin().Next().Current()==4);
    ASSERT_TRUE(it.begin().Next().Prev().Current()==1);
}
