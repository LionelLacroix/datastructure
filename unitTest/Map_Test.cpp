#include "../Map.h"
#include <gtest/gtest.h>
#include <string>

TEST(Map_insert, working_append)
{
    Map<std::string,int> mapping;
    mapping.insert(std::pair<std::string,int>("Test1",3));
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    ASSERT_TRUE(mapping["Test1"] == 3);
    ASSERT_FALSE(mapping["Test8"] == 8);
}


TEST(MapSize, working_size)
{
    Map<std::string,int> mapping;
    ASSERT_TRUE(mapping.isEmpty());
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    ASSERT_TRUE(mapping.size() == 2);
    //To Do bug
    //mapping.clear();
    //ASSERT_TRUE(mapping.isEmpty());
}


TEST(MapIterator, working_Iterator)
{
    Map<std::string,int> mapping;
    mapping.insert(std::pair<std::string,int>("Test1",3));
    mapping.insert(std::pair<std::string,int>("Test2",4));
    mapping.insert(std::pair<std::string,int>("Test3",7));
    mapping.insert(std::pair<std::string,int>("Test4",8));
    auto itFound = mapping.find("Test1");
    ASSERT_TRUE(itFound!= mapping.end());
    auto itFound1 = mapping.find("Test17");
    ASSERT_FALSE(itFound1!= mapping.end());

    auto it = mapping.iterator();
    ASSERT_TRUE(it.begin().second==3);
    ASSERT_TRUE(it.begin().first=="Test1");
    ASSERT_TRUE(it.begin().Next().second==4);
    ASSERT_TRUE(it.begin().Next().first=="Test2");
    ASSERT_TRUE(it.begin().Next().Prev().second==3);

}
