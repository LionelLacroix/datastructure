#include "../Stack.h"
#include <gtest/gtest.h>


TEST(StackPush_back, working_push)
{
Stack<int> test;
test.push(1);
test.push(4);


ASSERT_TRUE(test.front()==4);
test.pop_last();
test.pop_last();
ASSERT_TRUE(test.isEmpty());
}


TEST(StackSize, working_size)
{
Stack<int> test;
ASSERT_TRUE(test.isEmpty());
test.push(5);
test.push(2);
ASSERT_TRUE(test.size()==2);
ASSERT_FALSE(test.isEmpty());
}


TEST(StackIterator, working_iterator)
{
  Stack<int> test;
  test.push(1);
  test.push(4);
  test.push(5);
  Iterator<int, Stack<int>> it = test.iterator();
  int t = it.begin().current();
  ASSERT_TRUE(it.begin().current()==5);
  ASSERT_TRUE(it.begin().Next().current()==4);
  ASSERT_TRUE(it.begin().Next().Prev().current()==5);
}
